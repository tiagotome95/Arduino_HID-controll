# Arduino_HID-controll

Controll your Computer with an Arduino Leonardo, Pro Micro and/or other boards, that can act as an HID-device over USB.
This Software sends a text string by interaction with buttons and can automatize keyboard and mouse input, use it with care.


## Authors and acknowledgment

Tiago Simões Tomé <tiagotome95@gmail.com>

from Keyboard Message test:
  created 24 Oct 2011
  modified 27 Mar 2012
  by Tom Igoe

  modified 11 Nov 2013
  by Scott Fitzgerald

## License
The documentation in this project is licensed under the [Creative Commons Attribution-ShareAlike 4.0 license](https://choosealicense.com/licenses/cc-by-sa-4.0/), the source code content (also the source code included in the documentation) is licensed under the [GNU GPLv3 license](https://choosealicense.com/licenses/gpl-3.0/).

### Sources / Links
This example code is in the public domain.

https://www.arduino.cc/en/Tutorial/BuiltInExamples/KeyboardMessage

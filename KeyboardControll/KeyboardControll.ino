/*
  Keyboard Message test

  For the Arduino Leonardo and Micro. And the Pro Micro

  Sends a text string when a button is pressed.

  The circuit:
  - pushbutton attached from pin 4 to +5V
  - 10 kilohm resistor attached from pin 4 to ground

  created 24 Oct 2011
  modified 27 Mar 2012
  by Tom Igoe

  modified 11 Nov 2013
  by Scott Fitzgerald

  modified 25 Jul 2022
  by Tiago Simões Tomé
    - Just type out message:
      * when the interval since last button interaction >= 20
        &&
      * the button get HIGH
        
  This example code is in the public domain.

  https://www.arduino.cc/en/Tutorial/BuiltInExamples/KeyboardMessage
*/

#include "Keyboard.h"

const unsigned int buttonPin = 4;          // input pin for pushbutton
bool previousButtonState = LOW;   // for checking the state of a pushButton

unsigned long previousMillis0;
const long interval0 = 20;

void setup() {
  // make the pushButton pin an input:
  pinMode(buttonPin, INPUT);
  // initialize control over the keyboard:
  Keyboard.begin();
}

void loop() {
  // ask for actual millis (time)
  unsigned long currentMillis = millis();
  // read the pushbutton:
  bool currentButtonState = digitalRead(buttonPin);

  // if the time is gone
  if (currentMillis-previousMillis0 >= interval0) {
    
    // if the buttonState changes to HIGH,
    if (currentButtonState == HIGH && previousButtonState == LOW) {
      // type out a message
      Keyboard.print(" ");
      //Keyboard.println(counter);
    }
    // save the current button state for comparison next time:
    previousButtonState = currentButtonState;
    // update the currentMillis as previousMillis0 to compare when the time is gone
    previousMillis0 = currentMillis;
  }
  
}
